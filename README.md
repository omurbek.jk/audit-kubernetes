Audit CIS Benchmark Kubernetes Applicaiton
============================
This stack will perform CIS Benchmark Kubernetes application audits

## Description
This composite will audit kubernetes application for kubernetes CIS Benchmark

* ![DevSec CIS Kubernetes Benchmark](https://github.com/dev-sec/cis-kubernetes-benchmark "Inspec profile github link")

## Hierarchy



## Required variables with no default

**None**


## Required variables with default

**None**


## Optional variables with default

### `AUDIT_KUBERNETES_PROFILES_ALERT_LIST`:
  * description: 
  * default: cis-kubernetes-benchmark


## Optional variables with no default

### `FILTERED_OBJECTS`:
  * description: JSON object of string or regex of aws objects to include or exclude and tag in audit

## Tags
1. Audit
1. Best Practices
1. CIS

## Categories


## Diagram


## Icon


